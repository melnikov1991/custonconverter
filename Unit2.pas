unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VclTee.TeeGDIPlus, VclTee.TeEngine,
  VclTee.Series, Vcl.ExtCtrls, VclTee.TeeProcs, VclTee.Chart, Vcl.StdCtrls,
  Vcl.ComCtrls;

const
  n = 10;

type
  TUnit = record
  type
    TCoeff = record
      name: string;
      coeff: double;
    end;

  var
    name: string;
    coeffArr: array [0 .. n - 1] of TCoeff

    end;

  TUnitBase = record

    data: array [0 .. n - 1] of TUnit;

  function GetCoeff(unitIn, unitOut: string): double;

  end;

  TForm2 = class(TForm)
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    UpDown1: TUpDown;
    Label1: TLabel;
    Label3: TLabel;
    LabeledEdit1: TLabeledEdit;
    Label4: TLabel;
    Edit4: TEdit;
    Label2: TLabel;
    Label5: TLabel;
    Button1: TButton;
    UpDown2: TUpDown;
    procedure Button1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit3Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure UpDown1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure UpDown2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    unitBase: TUnitBase;
    { Private declarations }
  public
    procedure Calculate(Sender: TObject);
    procedure PrepareCmb;
    procedure PrepareDb;
    procedure ResetSrcValue;
    procedure UpdateLabel(cbx: TComboBox; edit: TEdit; lbl: TLabel);
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses
  System.Math;

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin

  var
  bufIndex := ComboBox1.itemindex;

  var
  bufValue := Edit1.Text;

  var
  updownPos := UpDown1.Position;

  ComboBox1.itemindex := ComboBox2.itemindex;
  Edit1.Text := '';
  // Edit1.Text := Edit2.Text;
  UpDown1.Position := UpDown2.Position;

  ComboBox2.itemindex := bufIndex;
  Edit2.Text := '';
  // Edit2.Text := bufValue;
  UpDown2.Position := updownPos;
  LabeledEdit1.Text := '';

  Calculate(self.Edit1);
  UpdateLabel(ComboBox1, Edit3, Label1);
  UpdateLabel(ComboBox2, Edit4, Label2);

end;

procedure TForm2.Calculate(Sender: TObject);
begin

  if Edit1.Text = '' then
  begin
    Edit2.Text := '';
    exit;
  end;

  if (Sender is TEdit) and ((Sender as TEdit).name = 'Edit1') then
  begin

    var
    coeff := self.unitBase.GetCoeff(ComboBox1.Text, ComboBox2.Text);

    var
    orderSrc := strtoint(Edit3.Text);
    var
    orderOut := strtoint(Edit4.Text);

    var
      multiplier: double := 1;

    if orderSrc > orderOut then

      multiplier := power(10, (orderSrc - orderOut))

    else if orderSrc < orderOut then
      multiplier := 1 / power(10, (-orderSrc + orderOut));

    var
    resCoeff := coeff * multiplier;

    var
    srcValue := strtoFloat(Edit1.Text);

    Edit2.Text := (srcValue * resCoeff).ToString;

    LabeledEdit1.Text := resCoeff.ToString;

  end

end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  Calculate(self.Edit1);
  UpdateLabel(ComboBox1, Edit3, Label1);
  ResetSrcValue;
end;

procedure TForm2.ComboBox2Change(Sender: TObject);
begin
  Calculate(self.Edit1);
  UpdateLabel(ComboBox2, Edit4, Label2);
end;

procedure TForm2.Edit1Change(Sender: TObject);
begin

  Calculate(self.Edit1);

end;

procedure TForm2.Edit1KeyPress(Sender: TObject; var Key: Char);
var
  ch: set of Char;
  EditLoc: TEdit;
  I: Integer;
begin
  ch := [formatSettings.decimalseparator];
  EditLoc := (Sender as TEdit);

  case Key of

    '0' .. '9', #8:
      ;

    ',', '.':
      begin

        Key := formatSettings.decimalseparator;

        with EditLoc do
        begin

          for I := 1 to Length(EditLoc.Text) do
            if EditLoc.Text[I] in ch then
              Key := Char(0);

          if EditLoc.Text = '' then
            Key := Char(0);

        end;

      end
  else
    Key := Char(0);

  end;
end;

procedure TForm2.Edit3Change(Sender: TObject);
begin

  // showmessage(edit3.Text)

end;

procedure TForm2.FormCreate(Sender: TObject);
begin

  PrepareDb;
  PrepareCmb;

  UpDown1.Position := 0;
  UpDown2.Position := 0;

  Calculate(self.Edit1);
  UpdateLabel(ComboBox1, Edit3, Label1);
  UpdateLabel(ComboBox2, Edit4, Label2);

end;

procedure TForm2.PrepareCmb;
begin

  for var I := 0 to High(unitBase.data) do
    if unitBase.data[I].name <> '' then
    begin
      ComboBox1.items.Add(unitBase.data[I].name);
      ComboBox2.items.Add(unitBase.data[I].name);
    end;

  if ComboBox1.items.Count <> 0 then
    ComboBox1.itemindex := 0;

  if ComboBox2.items.Count <> 0 then
    ComboBox2.itemindex := 0;

end;

procedure TForm2.PrepareDb;
begin

  unitBase.data[0].name := '���*�';
  unitBase.data[0].coeffArr[0].name := '����';
  unitBase.data[0].coeffArr[0].coeff := 0.00086;
  unitBase.data[0].coeffArr[1].name := '���*�';
  unitBase.data[0].coeffArr[1].coeff := 1;
  unitBase.data[0].coeffArr[2].name := '���';
  unitBase.data[0].coeffArr[2].coeff := 0.000123;
  unitBase.data[0].coeffArr[3].name := '���';
  unitBase.data[0].coeffArr[3].coeff := 3.6 / 1000;
  unitBase.data[0].coeffArr[4].name := '���.�^3 ����';
  unitBase.data[0].coeffArr[4].coeff := 0.108 / 1000;

  unitBase.data[1].name := '����';
  unitBase.data[1].coeffArr[0].name := '���*�';
  unitBase.data[1].coeffArr[0].coeff := 1163;
  unitBase.data[1].coeffArr[1].name := '����';
  unitBase.data[1].coeffArr[1].coeff := 1;
  unitBase.data[1].coeffArr[2].name := '���';
  unitBase.data[1].coeffArr[2].coeff := 0.143;
  unitBase.data[1].coeffArr[3].name := '���';
  unitBase.data[1].coeffArr[3].coeff := 4.187;
  unitBase.data[1].coeffArr[4].name := '���.�^3 ����';
  unitBase.data[1].coeffArr[4].coeff := 0.126;

  unitBase.data[2].name := '���';
  unitBase.data[2].coeffArr[0].name := '���*�';
  unitBase.data[2].coeffArr[0].coeff := 8130;
  unitBase.data[2].coeffArr[1].name := '����';
  unitBase.data[2].coeffArr[1].coeff := 7;
  unitBase.data[2].coeffArr[2].name := '���';
  unitBase.data[2].coeffArr[2].coeff := 1;
  unitBase.data[2].coeffArr[3].name := '���';
  unitBase.data[2].coeffArr[3].coeff := 29.31;
  unitBase.data[2].coeffArr[4].name := '���.�^3 ����';
  unitBase.data[2].coeffArr[4].coeff := 0.88;

  unitBase.data[3].name := '���';
  unitBase.data[3].coeffArr[0].name := '���*�';
  unitBase.data[3].coeffArr[0].coeff := 278;
  unitBase.data[3].coeffArr[1].name := '����';
  unitBase.data[3].coeffArr[1].coeff := 0.239;
  unitBase.data[3].coeffArr[2].name := '���';
  unitBase.data[3].coeffArr[2].coeff := 0.034;
  unitBase.data[3].coeffArr[3].name := '���';
  unitBase.data[3].coeffArr[3].coeff := 1;
  unitBase.data[3].coeffArr[4].name := '���.�^3 ����';
  unitBase.data[3].coeffArr[4].coeff := 0.03;

  unitBase.data[4].name := '���.�^3 ����';
  unitBase.data[4].coeffArr[0].name := '���*�';
  unitBase.data[4].coeffArr[0].coeff := 9239;
  unitBase.data[4].coeffArr[1].name := '����';
  unitBase.data[4].coeffArr[1].coeff := 7.950;
  unitBase.data[4].coeffArr[2].name := '���';
  unitBase.data[4].coeffArr[2].coeff := 1.136;
  unitBase.data[4].coeffArr[3].name := '���';
  unitBase.data[4].coeffArr[3].coeff := 33.287;
  unitBase.data[4].coeffArr[4].name := '���.�^3 ����';
  unitBase.data[4].coeffArr[4].coeff := 1;

end;

procedure TForm2.ResetSrcValue;
begin

  Edit1.Text := '1';

end;

procedure TForm2.UpdateLabel(cbx: TComboBox; edit: TEdit; lbl: TLabel);
begin

  if cbx.Text = '���*�' then
  begin

    case strtoint(edit.Text) of
      - 6:
        lbl.Caption := '������*�';
      -3:
        lbl.Caption := '��*�';
      0:
        lbl.Caption := '���*�';
      3:
        lbl.Caption := '���*�';
      6:
        lbl.Caption := '���.���*�';
      9:
        lbl.Caption := '����.���*�';
      12:
        lbl.Caption := '�����.���*�';
      15:
        lbl.Caption := '������*�';
      18:
        lbl.Caption := '������*�';

    else
      lbl.Caption := '';
    end;
    exit
  end;

  if cbx.Text = '����' then
  begin

    case strtoint(edit.Text) of

      - 12:
        lbl.Caption := '�������';
      -9:
        lbl.Caption := '���';
      -6:
        lbl.Caption := '����';
      -3:
        lbl.Caption := '����';
      0:
        lbl.Caption := '����';
      3:
        lbl.Caption := '���.����';
      6:
        lbl.Caption := '���.����';
      9:
        lbl.Caption := '����.����';
      12:
        lbl.Caption := '�����.����';

    else
      lbl.Caption := '';
    end;

    exit;
  end;
  if cbx.Text = '���' then
  begin

    case strtoint(edit.Text) of

      - 6:
        lbl.Caption := '���';
      -3:
        lbl.Caption := '���';
      0:
        lbl.Caption := '���';
      3:
        lbl.Caption := '���';
      6:
        lbl.Caption := '���';
      9:
        lbl.Caption := '������';
      12:
        lbl.Caption := '������';

    else
      lbl.Caption := '';
    end;

    exit;
  end;

  if cbx.Text = '���' then
  begin

    case strtoint(edit.Text) of

      - 6:
        lbl.Caption := '��.�.�.';
      -3:
        lbl.Caption := '��.�.�.';
      0:
        lbl.Caption := '�.� � ';
      3:
        lbl.Caption := '���.�.� �';
      6:
        lbl.Caption := '���.�.� �';
      9:
        lbl.Caption := '����.�.� �';
      12:
        lbl.Caption := '�����.�.� �';

    else
      lbl.Caption := '';
    end;
  end;

  if cbx.Text = '���.�^3 ����' then
  begin

    case strtoint(edit.Text) of

//      - 6:
//        lbl.Caption := '��.�.�.';
      -3:
        lbl.Caption := '�^3 ����';
      0:
        lbl.Caption := '���.�^3 ����';
      3:
        lbl.Caption := '���.�^3 ����';
      6:
        lbl.Caption := '����.�^3 ����';
//      9:
//        lbl.Caption := '����.�.� �';
//      12:
//        lbl.Caption := '�����.�.� �';

    else
      lbl.Caption := '';
    end;

    exit;
  end;

end;

procedure TForm2.UpDown1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Calculate(Edit1);

  UpdateLabel(ComboBox1, Edit3, Label1);

end;

procedure TForm2.UpDown2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Calculate(Edit1);
  UpdateLabel(ComboBox2, Edit4, Label2);
end;

{ TUnitBase }

function TUnitBase.GetCoeff(unitIn, unitOut: string): double;
begin

  var
  matchbool := false;

  for var I := Low(self.data) to High(self.data) do
    if self.data[I].name = unitIn then
    begin

      var
      match := I;

      for var j := Low(self.data[match].coeffArr)
        to High(self.data[match].coeffArr) do
        if self.data[match].coeffArr[j].name = unitOut then
        begin
          exit(self.data[match].coeffArr[j].coeff)
        end;

    end;

  if not matchbool then
    raise Exception.Create('�����. �� ������');

end;

end.
