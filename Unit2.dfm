object Form2: TForm2
  Left = 0
  Top = 0
  Caption = #1050#1086#1074#1077#1088#1090#1077#1088' '#1101#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1080#1093' '#1074#1077#1083#1080#1095#1080#1085
  ClientHeight = 179
  ClientWidth = 407
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 18
  object Label1: TLabel
    Left = 310
    Top = 36
    Width = 41
    Height = 18
    Caption = 'Label1'
  end
  object Label3: TLabel
    Left = 174
    Top = 77
    Width = 5
    Height = 18
  end
  object Label4: TLabel
    Left = 114
    Top = 35
    Width = 27
    Height = 18
    Caption = '10^'
  end
  object Label2: TLabel
    Left = 310
    Top = 128
    Width = 41
    Height = 18
    Caption = 'Label1'
  end
  object Label5: TLabel
    Left = 113
    Top = 125
    Width = 27
    Height = 18
    Caption = '10^'
  end
  object ComboBox1: TComboBox
    Left = 23
    Top = 32
    Width = 85
    Height = 26
    Style = csDropDownList
    TabOrder = 0
    OnChange = ComboBox1Change
  end
  object ComboBox2: TComboBox
    Left = 23
    Top = 122
    Width = 85
    Height = 26
    Style = csDropDownList
    TabOrder = 1
    OnChange = ComboBox2Change
  end
  object Edit1: TEdit
    Left = 183
    Top = 33
    Width = 121
    Height = 26
    TabOrder = 2
    OnChange = Edit1Change
    OnKeyPress = Edit1KeyPress
  end
  object Edit2: TEdit
    Left = 184
    Top = 122
    Width = 121
    Height = 26
    ReadOnly = True
    TabOrder = 3
  end
  object Edit3: TEdit
    Left = 142
    Top = 33
    Width = 19
    Height = 26
    ReadOnly = True
    TabOrder = 4
    Text = '0'
    OnChange = Edit3Change
  end
  object UpDown1: TUpDown
    Left = 161
    Top = 33
    Width = 16
    Height = 26
    Associate = Edit3
    Min = -20
    Max = 20
    TabOrder = 5
    OnMouseDown = UpDown1MouseDown
  end
  object LabeledEdit1: TLabeledEdit
    Left = 183
    Top = 74
    Width = 121
    Height = 26
    EditLabel.Width = 119
    EditLabel.Height = 18
    EditLabel.Caption = #1055#1077#1088#1077#1074#1086#1076'. '#1082#1086#1101#1092#1092'.'
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 6
  end
  object Edit4: TEdit
    Left = 142
    Top = 122
    Width = 19
    Height = 26
    ReadOnly = True
    TabOrder = 7
    Text = '0'
  end
  object Button1: TButton
    Left = 23
    Top = 75
    Width = 34
    Height = 25
    Caption = '||'
    TabOrder = 8
    OnClick = Button1Click
  end
  object UpDown2: TUpDown
    Left = 161
    Top = 122
    Width = 16
    Height = 26
    Associate = Edit4
    Min = -20
    Max = 20
    TabOrder = 9
    OnMouseDown = UpDown2MouseDown
  end
end
